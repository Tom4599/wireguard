# Utilisation de Wireguard

## Installation de Wireguard (Cette documentation est pour debian sinon allez [ici](https://www.wireguard.com/install/)) (en sudo et à faire sur chaque machine)
```
# echo "deb http://deb.debian.org/debian/ unstable main" > /etc/apt/sources.list.d/unstable.list
# printf 'Package: *\nPin: release a=unstable\nPin-Priority: 90\n' > /etc/apt/preferences.d/limit-unstable
# apt update
# apt install wireguard
```

## Génération clés (en sudo et à faire sur chaque machine)
```
# cd /etc/wireguard/
# umask 077
# wg genkey > privatekey
# wg pubkey < privatekey > publickey 
```
## Création du fichier /etc/wireguard/wg0.conf
Trois méthodes sont expliquées ici : Site à Site, RoadWarrior et Serveur dans le LAN.
- Site à Site permet comme son nom l'indique de relié deux réseaux entre eux avec un tunnel sécurisé.
- RoadWarrior permet de connecter une machine directement à un serveur pour accéder au réseau LAN derrière celui-ci.
- Serveur dans le LAN est la configuration lorsque le client est dans internet et qu'il souhaite se connecter au serveur situé dans le LAN derrière un pare-feu.

### Site à Site
wg0.conf sur le premier pare-feu
```
[Interface]
ListenPort = 45947 
Address = 10.0.0.1/24
PrivateKey = ePkO5/wmCknBOrtRdgbIVhSj/rjeP14cr7yOyc2Djmo=

[Peer]
PublicKey = /2rWHggulgBGERvOztjHn8VxyG+UxKFgZbG8JTf5A3I=
AllowedIPs = 10.0.0.2/32, 10.0.12.0/24
Endpoint = 192.168.1.2:46094
PersistentKeepalive = 25
```
wg0.conf sur le deuxième pare-feu
```
[Interface]
ListenPort = 46094
Address = 10.0.0.2/24
PrivateKey = qNNaXUHtDSK9hlhheyxYg3/yIcybazVGZiXOp4RIimM=

[Peer]
PublicKey = zK0B4gASY9PO1gITyZjZjz8tbXNrHfZpZar+Ca81XXc=
AllowedIPs = 10.0.0.1/32, 10.0.11.0/24
Endpoint = 192.168.1.1:45947
PersistentKeepalive = 25
```
### Explications
Pour chaque fichier il faut spécifier la partie [Interface]. Un port d'écoute, une IP (celle de la future interface wg0 qui sera créée) et la private key. Vous pouvez la trouver comme ceci :
```
# cat /etc/wireguard/privatekey
```
Il faut également donner les informations du peer, c'est à dire de l'autre serveur avec qui on sera connecté. Sa public key (`cat /etc/wireguard/publickey` sur l'autre machine), les IP autorisées (Il faut mettre l'IP du peer avec un /32 et également le réseau qui est derrière le pare-feu). Spécifié l'Endpoint, c'est à dire l'ip et le port sur lequel joindre directement le peer puis la dernière ligne sert à garder la connexion active.
**N'oubliez pas de changer ces informations suivants votre cas.**

### RoadWarrior
wg0.conf  sur le serveur VPN
```
[Interface]
ListenPort = 45000
Address = 10.0.0.1/24
PrivateKey = KM8GDpi9MymoU1AmbfI31iPEd8gx/TrelHqaqvXk9EM=

[Peer]
PublicKey = hNJEyK7FzaUOevwzE/mSFX98uB9fEnLcPZBjITlq+FA=
AllowedIPs = 10.0.0.2/32
Endpoint = 10.0.1.1:46000
PersistentKeepalive = 25
```
wg0.conf sur le client VPN
```
[Interface]
ListenPort = 46000
Address = 10.0.0.2/24
PrivateKey = QBA7c1Hl/VpA4NBQbiL5ELXFxcpYgdZGSFSEtFJIGlE=

[Peer]
PublicKey = wbcQA7c3Xo1JM/lQKORGrNeQP/KjW4v9MypLDw5NaCE=
AllowedIPs = 10.0.0.1/32, 192.168.1.0/24
Endpoint = 10.0.1.254:45000
PersistentKeepalive = 25
```
Les informations données sont les mêmes que celles expliquées au-dessus. La seule différence étant que dans le fichier du serveur, on a pas à spécifié de deuxième réseau car le client n'a pas de réseau derrière lui.
### Serveur dans le LAN
wg0.conf  sur le serveur VPN
```
[Interface]
PrivateKey = 4A/NhWz2729oUcdVwDV7m5qSNJ+hom/kktVZIs3Le3U=
Address = 10.0.0.1
ListenPort = 45000
PostUp = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o enp0s7 -j MASQUERADE
PostDown = iptables -D FORWARD -i wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o enp0s7 -j MASQUERADE


[Peer]
PublicKey = hNJEyK7FzaUOevwzE/mSFX98uB9fEnLcPZBjITlq+FA=
AllowedIPs = 10.0.0.2/32
PersistentKeepalive = 25
```
wg0.conf sur le client VPN
```
[Interface]
ListenPort = 46000
Address = 10.0.0.2/24
PrivateKey = QBA7c1Hl/VpA4NBQbiL5ELXFxcpYgdZGSFSEtFJIGlE=

[Peer]
PublicKey = qDnARr/0rrL5OO4ZScDTc5y7x3W3A+q5cxNXolmr/VQ=
AllowedIPs = 0.0.0.0/0
Endpoint = <PUBLIC_SERVER_IP>:45000
PersistentKeepalive = 25
```
Les informations données sont les mêmes que celles expliquées au-dessus. Les seules différences sont que dans le fichier du serveur il faut executer une commande iptables pour faire du NAT sur l'interface LAN du serveur (cela permet au client d'accéder à internet comme s'il était connecté directement dans le LAN).  De plus, dans le fichier du client "AllowedIPs = 0.0.0.0/0" signifie que l'on autorise IP car le client peut être connecté de n'importe où.
## Connexion 
Pour cela rien de plus simple il suffit d’exécuter la commande suivante sur les deux machines :
```
# wg-quick up wg0
``` 
Vous pouvez alors vérifiez la connexion grâce à cette commande :
```
# wg
interface: wg0
  public key: hNJEyK7FzaUOevwzE/mSFX98uB9fEnLcPZBjITlq+FA=
  private key: (hidden)
  listening port: 46000

peer: wbcQA7c3Xo1JM/lQKORGrNeQP/KjW4v9MypLDw5NaCE=
  endpoint: 10.0.1.254:45000
  allowed ips: 192.168.1.0/24, 10.0.0.1/32
  latest handshake: 3 seconds ago
  transfer: 6.61 KiB received, 2.64 KiB sent
  persistent keepalive: every 25 seconds
```
Vous pouvez alors accéder à votre LAN ou autre Site à distance comme en étant sur place avec un accès sécurisé.

On peut également remettre à la fin le umask par défaut
```
# umask 022
```

